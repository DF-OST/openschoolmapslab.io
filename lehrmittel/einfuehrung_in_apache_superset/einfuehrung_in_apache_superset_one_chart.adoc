= Einführung in Apache Superset: Ein Chart
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten
:imagesdir: ../../bilder/
:xrefstyle: short
:experimental:
include::../../snippets/lang/de.adoc[]
include::../../snippets/suppress_title_page.adoc[]

*Ein Arbeitsblatt für Interessierte und Lehrpersonen*

== Übersicht

=== Lernziele

Diese Einführung hat folgende Lernziele:

* Du kennst die wichtigsten Konzepte und Grundfunktionen von Apache Superset
* Du kannst Datenquellen selektieren
* Du kannst Daten mittels Charts visualisieren
* Du kannst Charts zu einem Dashboard anordnen
* Du kannst Dashboards mit anderen (übers Web) teilen

Das Bearbeiten dieses Arbeitsblatts dauert ca. eine halbe Stunde, je nach deinem Vorwissen.

Für diese Einführung werden keine Programmierkenntnisse vorausgesetzt;
Grundkenntnisse der Tabellenkalkulation genügen fürs Erste.

Für die Übungen in diesem Arbeitsblatt benötigst du Zugang zu einem Apache Superset-Service (vgl. unten),
sowie einen gängigen Webbrowser (am besten funktioniert Superset auf Chrome) und eine Internetverbindung.

Diese Anleitung bezieht sich auf Release 2.0 von Apache Superset.

=== Einleitung

Für den Erfolg einer Entscheidungsfindung ist eine gute Visualisierung wichtig.
Darum müssen die Erkenntnisse eines Anliegens, Projektes oder Umfrage visualisiert werden.
Wenn Erkenntnisse anschaulich und nachvollziehbar dargestellt werden, erhöht das deren Verständlichkeit und Akzeptanz.

Visualisierung kann man zudem nicht nur zur Veranschaulichung einsetzen, sondern auch zur Datenanalyse.
Häufig erkennt man Zusammenhänge in den Daten erst durch eine geschickte Darstellung.
Wir Menschen sind schlecht darin, Zahlen zu vergleichen; grafische Muster dagegen erkennen wir gut.
Visualisierung stellt somit nicht nur die Daten grafisch dar, sondern kann auch als eigene Technik der Datenanalyse eingesetzt werden.
Das Internet ermöglicht zudem die Publikation der Visualisierung und damit die einfache Kommunikation mit Kunden und Arbeitskollegen.

Apache Superset ist so ein Daten-Visualisierungs- und Publikations-Werkzeug.
Manche nennen diese Anwendung auch "Business Intelligence Tool".
Superset ist auch ein Werkzeug zum Teilen (Sharing) von Datenquellen, d.h. von Tabellen-Daten bis zu Geodaten.
Es kann mit verschiedensten Datenbanken verbunden werden.

[NOTE]
--
Zu Apache Superset gibt es eine offizielle, englischsprachige https://superset.apache.org/docs/intro[Dokumentation].

Das https://superset.apache.org/docs/frequently-asked-questions[FAQ] (Frequently Asked Questions, Deutsch: "häufig gestellte Fragen"), welches auf der Seite zu finden ist, enthält teils nützliche Informationen.
--



=== Benutzerkonto erstellen/anmelden

Apache Superset ist eine Webapplikation und für den Zugang wird je nach Server ein Konto benötigt.
Wenn du bereits Zugang zu einer Apache-Superset-Instanz hast, dann melde dich dort an.

Apache Superset kennt verschiedene Benutzer-Rollen, die bestimmte Rechte haben, um Daten zu verändern oder Funktionen aufzurufen.
In diesem Arbeitsblatt wird angenommen, dass du Benutzerrechte erhalten hast, die der "Gamma"-Rolle entsprechen, d.h. du kannst Charts und Dashboards erstellen.

NOTE: Alternativ kann auch lokal eine Apache-Superset-Instanz mit https://www.docker.com/[Docker] aufgesetzt werden. Hierzu gibt es ein Tutorial auf der https://superset.apache.org/docs/installation/installing-superset-using-docker-compose/[offiziellen Superset-Website].


== Konzepte und Begriffe

Nach dem Anmelden an einer Apache Superset-Instanz (siehe vorhergehendes Kapitel), wird die Startseite von Apache-Superset angezeigt.

[[abb_landing_screen]]
.Startbildschirm von Apache-Superset.
image::einfuehrung_in_apache_superset/Superset_Startbildschirm.png[]


Hier einige Erläuterungen zu den Konzepten hinter Apache Superset:

Data (Datenquelle):: Dies sind Datenbanken, welche anschliessend ausgewertet und verarbeitet werden. Auf diese können später dann auch SQL-Abfragen getätigt werden.
Chart (Diagramm):: In einem _Chart_ werden die Daten aus einem Dataset grafisch dargestellt.
Dashboard:: Eine Zusammenstellung aus verschiedenen Charts, welche als Website aufgerufen werden kann. 
Metric:: Eine "Metric", manchmal auch ein "Measurement" genannt, ist eine numerische Kennzahl. Metrics / Measurements werden v.a. in den Charts erwähnt und verlangt.
Record:: Datenquellen und Charts sind alles Programmierelemente, die manchmal in der Benutzeroberfläche als "Record" bezeichnet werden.
SQL-Query:: Anweisung, Datenbankanfrage in der Datenbanksprache SQL. SQL-Queries kann man speichern und als Datenquelle anderen zur Verfügung stellen.

== Daten und Fragestellungen

Die mit Apache Superset (und Business Intelligence Tools allgemein) zu visualisierenden Daten müssen in strukturierter und sauberer Form vorliegen.
Wenn nötig, müssen die Daten mit Datenbanksystemen (SQL), Tabellenkalkulationsprogrammen (z.B. MS Excel, LibreOffice) oder GIS (z.B. QGIS) aufbereitet werden
(siehe u.a. https://openschoolmaps.ch/[OpenSchoolMaps] > "Einführung in QGIS 3 und in Geoinformationssysteme (GIS)").


NOTE: Für die Bereinigung von Daten ("Data Wrangling") gibt es http://openrefine.org/[OpenRefine], welches ein Programm ist, welches den Prozess erheblich vereinfacht. Hierzu gibt es ebenfalls ein Arbeitsblatt auf https://openschoolmaps.ch/[OpenSchoolMaps] > "Daten sichten, bereinigen und integrieren mit OpenRefine".

In diesem Arbeitsblatt wird nur eine Tabelle verwendet, die Tabelle `wb_health_population` von der Weltbank (https://github.com/apache/incubator-superset/blob/master/superset/examples/countries.md[Quelle], Lizenz CC BY-4.0, Stand ca. 2017, übersetzt etwa "Weltbank-Gesundheit-Bevölkerung").

Die Tabelle `wb_health_population` hat ca. 328 Spalten (Attribute), d.h. sehr viele.
Wir verwenden davon folgende Spalten:

* `country_name`: Name des Landes
* `region`: Weltregion, in der das Land liegt
* `year`: Jahr der Datenerhebung (1960 - 2014)
* `SP_POP_TOTL`: Anzahl Einwohner insgesamt

<<abb_wb_health_population>> zeigt die Daten, die wir nutzen werden.
Nimm dir doch kurz Zeit und schaue dir diese Daten genau an.
Zu verstehen, welche Daten in welcher Spalte sind, ist eine Notwendigkeit, um sinnvolle und korrekte Diagramme erstellen zu können.

[[abb_wb_health_population]]
.Daten der Schweiz von 1960 - 1978 aus der Tabelle `wb_health_population`.
image::einfuehrung_in_apache_superset/wb_health_population_table.png[, 860, 535]


== Charts (Diagramme)

Apache Superset bietet eine Vielzahl an verschiedenen Diagrammen. Im Anhang findest du weitere Beispieldiagramme zur Tabelle `wb_health_population`, die in weiterführenden Arbeitsblättern vorgestellt werden (Siehe Kapitel "Abschluss").


Ziel dieser Aufgabe ist es, ein Dashboard zu erstellen, auf welchem sich ein Filter und ein Diagramm befindet. Das Diagramm wird mithilfe des Filters beeinflussbar sein. 

[[abb_Superset_Kurzeinfuehrung_Dashboard_1]]
.Das Dashboard mit dem Kreis-Diagramm und dem Filter.
image::einfuehrung_in_apache_superset/Superset_Kurzeinfuehrung_Dashboard.png[, 860, 535]

NOTE: Wenn du Änderungen an deinem Diagramm vornimmst, kannst du diese mit einem Klick auf btn:[Update Chart] aktualisieren.

=== Aufgabe 1: Mein erster Chart

Um ein neues Diagramm zu erstellen, klicke auf menu:Charts[+ Chart].

.Knopf zum Erstellen eines neuen Charts.
image::einfuehrung_in_apache_superset/chart_erstellen.png[]

In dem neuen Fenster muss nun zunächst das gewünschte _Dataset_ gewählt werden. Im Fall dieser Übung ist das `wb_health_population`.

NOTE: Man kann jeweils nur eine Tabelle als Quelle auszuwählen.
Möchte man mehrere Tabellen zu einer Datenquelle verknüpfen, benötigt man SQL-Kenntnisse. In weiteren Arbeitsblättern auf OpenSchoolMaps wird gezeigt, wie das geht.

Sobald ein Datenset ausgewählt wurde, kann nun in einem zweiten Schritt der gewünschte Diagrammtyp gewählt werden. Für diese Aufgabe wird ein `Pie Chart` (Kreis- oder auch Kuchendiagramm) benötigt.

Die Auswahl kann mit einem Klick auf btn:[CREATE NEW CHART] bestätigt werden

.Oberfläche zur Konfiguration eines Charts.
image::einfuehrung_in_apache_superset/chart_einstellen.png[]

Nun muss noch eingestellt werden, welche Werte aus den gewählten Tabellen verwendet werden sollen.
Hierzu kann zunächst im Bereich menu:Time[TIME RANGE] der Bereich abgesteckt werden, welcher im Diagramm angezeigt werden soll.

Hier zu sollte der menu:RANGE TYPE[] von `No filter` auf `Custom` umgestellt werden. 

Nun kann die Zeit umgestellt werden. Hierbei kann zwischen verschiedenen Optionen gewählt werden. 
Es können entweder relative Zeitabstände (`Relative Date/Time`), absolute Zeitabstände (`Specific Date/Time`), bis zum Ende des Tages (`midnight`) oder bis jetzt (`now`) gewählt werden.

NOTE: Um alle Daten zu erhalten - egal zu welchem Jahr sie gehören - oder wenn es keine Daten gibt, die zeitabhängig sind, kann man `No filter` wählen.

Für dieses Projekt sollen die Datensätze vom `1.01.2014` bis zum `31.12.2014` ausgewertet werden. Also stelle dementsprechend den Filter ein.

WARNING: Wen ein absoluter Zeitraum ausgewählt werden muss die Eingabe mit einem Klick auf btn:[OK] bestätigt werden. Ansonsten wird der Wert nicht übernommen.

Unter dem Punkt menu:Query[] kann nun eingestellt werden, welche Daten das Diagramm auswerten soll. Als erstes muss ausgewählt werden auf welche Werte das Diagramm aufgeteilt wird. (In welche Kuchenstücke das Diagramm aufgeteilt werden soll). 
In diesem Beispiel sollen die einzelnen Abschnitte die Länder repräsentieren. Daher sollte hier `country_name` eingetragen werden.

Das Diagramm kann jedoch erst erstellt werden, wenn der Wert menu:Metric[] definiert wurde. Die _Metric_ definiert den Wert, an welchem die Grösse der einzelnen Felder berechnet wird.
In unserem Fall wollen wir die Bevölkerungsgrösse der Länder herausfinden, darum wird hier die Bevölkerungsgrösse eingefüllt.

Um diesen Auszuwählen, klicke auf menu:METRIC[SIMPLE > COLUMN] und wähle `#SP_POP_TOTL`.
Zusätzlich zu der Spalte muss noch der Wert menu:AGGREGATE[] gesetzt werden.
In unserem Fall suchen wir die Summer der Einträge.
Aus diesem Grund soll `SUM` gewählt werden. 

Unter menu:Row Limit[] kannst du das Ergebnis auf eine bestimmte Anzahl Einträge beschränken.
Wenn du z.B. ein _Row Limit_ von 10 setzt,
werden nur die 10 bevölkerungsreichsten Länder angezeigt.

Wenn du jetzt btn:[CREATE CHART] drückst, wird die Abfrage ausgeführt.
Sie zeigt in einem Kreis-Diagramm die zehn bevölkerungsreichsten Länder im Jahre 2014.

Nun kann oben im Bereich menu:Add the name of the chart[] ein Name für das Chart gewählt werden und das Diagramm mit btn:[SAVE] (oben rechts) gespeichert werden.

ifdef::show_solutions[]
====
.Lösung
Der fertige Stand muss wie folgt aussehen:

image::einfuehrung_in_apache_superset/Superset_Kurzeinfuehrung_aufg1_loesung.png[pdfwidth=85%]
====
endif::show_solutions[]


=== Aufgabe 2: Filter Box für das Dashboard

Als nächstes möchten wir ein Filter für den Datensatz erstellen. Diese können genauso wie ein Diagramm erstellt werden. Also navigiere zu menu:Charts[+ CHART].
Das Dataset ist wie bereits in der ersten Aufgabe `wb_health_population`.

Als Chart-Type sollte diesmal jedoch `Filter Box` gewählt werden. Nun können im Bereich menu:Filter configuration[FILTERS] die Felder ausgewählt werden, welche gefiltert werden sollen. 

Im Fall der Aufgabe kann hier die Spalte `country_name` ausgewählt werden.

Nun kann die Filter Box genauso wie ein Diagramm auch gespeichert werden.

ifdef::show_solutions[]
====
.Lösung
Die Abfrage müsste wie folgt aussehen:

image::einfuehrung_in_apache_superset/Superset_Kurzeinfuehrung_aufg2_loesung.png[pdfwidth=85%]
====
endif::show_solutions[]

== Charts zu einem Dashboard anordnen

Im Reiter Dashboard kann unter menu:Dashboards[+ DASHBOARD] ein neues Dashboard erstellt werden.

.Ansicht für ein neues Dashboard
image::einfuehrung_in_apache_superset/Superset_dashboards.png[]

Im Bereich menu:Untitled dashboard[] kann dann dem Dashboard ein Name gegeben werden.

Bei einem neu erstellten Dashboard kann man direkt zu editieren anfangen.
In jedem anderen Fall musst du oben rechts auf _Edit dashboard_ klicken.
Momentan ist dein Dashboard noch leer, jedoch kannst du dieses einfach per Drag & Drop füllen.
Die erste Komponente musst du nach oben zum Rand ziehen.
Wenn eine Komponente platziert werden kann, wird dies durch eine blaue Linie signalisiert, die zugleich anzeigt, wie/wo die Komponente platziert wird.
Zuerst klickst du dafür auf die _Edit dashboard_-Schaltfläche wodurch alle Komponenten angezeigt werden, die du hinzufügen kannst.

An Elementen unterscheidet man im Wesentlichen zwischen Layout-Elementen und Charts. Layout-Elemente sind dabei vorgefertigte Elemente, welche rein der Gestaltung des Dashboards dienen, also nicht mit den Daten interagieren. Die Charts hingegen sind entweder Diagramme oder Filter, also Elemente, welche die Daten entweder filtern oder visuell Darstellen. In diesem Bereich findet man auch alle User-Erstellten Elemente, z.B. die in der vorherigen Aufgabe erstellten Elemente.

Die Elemente können alle interaktiv umhergeschoben sowie vergössert und verkleinert werden.

=== Aufgabe 3: Ein Dashboard erstellen

_Erstelle nun ein Dashboard, das der <<abb_Superset_Kurzeinfuehrung_Dashboard_2>> entspricht._

[[abb_Superset_Kurzeinfuehrung_Dashboard_2]]
.Hier nochmals das Dashboard als Ergebnis dieses Arbeitsblatts mit dem Pie-Chart und dem Filter
image::einfuehrung_in_apache_superset/Superset_Kurzeinfuehrung_Dashboard.png[, 860, 535]

== Dashboards teilen
Sobald du mit deinem Dashboard zufrieden bist, kannst du es publizieren und/oder teilen.

Um ein Dashboard zu teilen, musst du hinter der menu:EDIT DASHBOARD[]-Schaltfläche auf das Dropdown-Menü und dort auf menu:...[Share > Copy Permalink to Clipboard].
Die URL kannst du nun einer anderen Person schicken, jedoch muss der Account dieser Person der entsprechenden Rolle zugewiesen sein.

== Abschluss

Geschafft! Du solltest nun ein Dashboard zu den Weltbank-Daten haben, das du anderen zeigen kannst.

TIP: Tipp zum Filter: In einem Zeit-Filter unter _Custom_ ist es möglich, direkt Jahreszahlen zu schreiben. Das Datum ist dann automatisch der erste Januar.

Wer mehr über Apache Superset erfahren will, dem seien die ausführlicheren Informationsblätter "Einführung in Apache Superset (7 Charts)" und
"Apache Superset für Fortgeschrittene" auf OpenSchoolMaps empfohlen.

.Diagramme, die in der Einführung in Apache Superset (7 Charts) vorgestellt werden.
image::einfuehrung_in_apache_superset/alle_diagramme.PNG[, 860, 535]

---

include::../../snippets/kontakt_openschoolmaps.adoc[]

include::../../snippets/license.adoc[]
