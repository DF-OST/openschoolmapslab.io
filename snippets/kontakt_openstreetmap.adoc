ifdef::hoeflichkeitsform[]
Noch Fragen? Wenden Sie sich an die https://osm.ch/hilfe.html[OpenStreetMap-Community]!
endif::[]

ifndef::hoeflichkeitsform[]
Noch Fragen? Wende dich an die https://osm.ch/hilfe.html[OpenStreetMap-Community]!
endif::[]
